# Mini9
## Gitlab repo: https://gitlab.com/iikikk/mini9/-/tree/main?ref_type=heads

## Requirement:
 ·  Create a website using Streamlit
 ·   Connect to an open source LLM (Hugging Face)
 ·   Deploy model via Streamlit or other service (accessible via browser)

## Steps

Step 1: Install necessary libraries
pip install streamlit transformers

Install the necessary Python libraries in requirements.txt using pip:
pip install -r requirements.txt

Step 2: Create the Streamlit App
Create main.py to implement the app function:

Step 3: Import necessary packages:

import streamlit as st
from transformers import pipeline

Step 4: Load the LLM model
generator = pipeline('text-generation', model='gpt2')

Step 5: implement user input and buttons.

Step 6: generate result and desplay

Step 7: Test Streamlit app locally:
streamlit run app.py

Step 8: upload it on github and deploy it on streamlit sharing

After the successful deployment, we can get the URL of the app:
https://ids721hw9-njc7jajjb4b73tqdnrdhet.streamlit.app/


## Stream shot:
 ![Alt text](image.png)
